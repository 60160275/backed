const mongoose = require('mongoose')
const Scheme = mongoose.Schema
const userScheme = new Scheme({
  name: String,
  gender: String
})
module.exports = mongoose.model('User', userScheme)
