const userController = {
  userList: [
    {
      id: 1,
      name: 'Worawit',
      gender: 'M'
    },
    {
      id: 2,
      name: 'Prasob',
      gender: 'M'
    }
  ],
  lastID: 3,
  addUser (user) {
    user.id = this.lastID++
    this.userList.push(user)
    return user
  },
  updataeUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
    return user
  },
  deleteUser (id) {
    const index = this.userList.findIndex(item => item.id === parseInt(id))
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  },
  getUser (id) {
    const user = this.userList.findIndex(item => item.id === parseInt(id))
    return user
  }
}

module.exports = userController
